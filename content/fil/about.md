+++
  title = "Tungkol sa Diseases Central @Welfareness"
  description = "Ang Diseases Central @Welfareness ay isang hub para sa pinakasariwang kaalaman patungkol sa iba't-ibang mga karamdaman."
  summary = "Ang Diseases Central @Welfareness ay isang hub para sa pinakasariwang kaalaman patungkol sa iba't-ibang mga karamdaman."    # for summary text in list displays
  author = "Welfareness"

  date = "2020-08-10T06:01:00+08:00"
  #expiryDate = ""
  #lastmod = ""    # automatic, do not use unless necessary
  #publishDate = ""    # date var should be enough

  aliases = ["tungkolsaamin", "kontak"]
  slug = "tungkol"
  translationKey = "about"

  #"mga backteriyum" = [""]
  #"mga kategorya" = [""]   #taxonomy
  #"mga apeksyon" = [""]
  keywords = ["diseases", "viruses", "bacteria", "microbes", "sickness", "plagues", "pandemic", "epidemic", "outbreak", "covid19", "covid-19", "coronavirus", "wuhan", "wuhanvirus", "sars"]   # meta keywords
  #"mga serye" = [""]   # subset of series taxonomy
  #"mga tag" = [""]   # taxonomy
  #"mga virus" = [""]

  #audio = ["/path/", "/path/"]
  #images = ["/path/", "/path/"]
  #videos = ["/path/", "/path"]

  #weight = ""

  #draft = true

  #featured = true    # only for some themes
  #math = true   # only for some themes
  #thumbnail = "images/building.png"   # only for some themes

  # https://gohugo.io/content-management/front-matter/#front-matter-cascade
  #[cascade]
  #  banner = "/path/"
+++

Ang **Diseases Central @Welfareness** ay isang hub para sa paglilikom ng mga sariwang kaalaman patungkol sa iba't-ibang mga karamdaman. Ito ay nagsimula bilang isang pagtugon sa napakaraming impormasyon na kumakalat ***online***, sa mga telebisyon at radyo, at sa mga bulung-bulungan, patungkol sa pandemya na karamdaman na tinatawag na *COVID-19*. Ang mga kaalaman na tunay na kapakipakinabang ay kadalasang nalilimutan at hindi na ito matagpuan muli kapag dumating ang panahon na ang impormasyon na ito ay angkop na angkop na.

Sa pamamagitan ng **Diseases Central @Welfareness** umaasa kami na makokolekta ang mga impormasyon na nabanggit sa iisang lugar. Sisimulan ang proyektong ito sa karamdaman na *COVID-19* sa kadahilanang ito ang kasalukuyang pandemya na ating dinaranas. Kalaunan, ang impormasyon sa mga ibang karamdaman ay isasama dito sa **Diseases Central @Welfareness**.

## Kami ay hindi …
Kami ay hindi isang *repository* para sa mga *research paper* at hindi rin isang *archive system*. Hindi rin namin ilalagay o ililista ang bawat impormasyon, ang aming hangarin ay para sa mga kaalaman na kadalasang nalilimutan at hindi na matagpuan kung kailan ito ay tunay na kailangan.

## Ano ang aming mapapangako …
Aming mapapangako na bawat impormasyon ay may kaloob na mga *citation* upang bigyan ang mambabasa ng pagkakataon upang magpasya sa sarili nila kung amin bang naintindihan ng tama ang isang *research paper*. Kami rin ay maaaring magbigay ng *link* sa mga iba pang balita at artikulo (na hindi *research paper*) subalit hindi ito nangangahulugan na kami ay sumasangayon sa lahat o alin man sa kanilang inilathala (maliban na lamang kung sa amin rin nanggaling iyon).
