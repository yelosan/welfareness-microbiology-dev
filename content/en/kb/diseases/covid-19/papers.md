+++
title = "List of COVID-19 Research Papers"
description = "A list of research papers about the COVID-19 disease."
summary = "A list of research papers about the COVID-19 disease."    # for summary text in list displays
author = "Welfareness"

date = "2020-08-10T07:08:17+08:00"

keywords = ["covid19", "covid-19", "coronavirus", "wuhanvirus", "sars-cov-2", "research", "papers", "disease"]   # meta keywords

aliases = ["covid19research", "covid19paper"]
slug = "papers"
translationKey = "covid19papers"

#bacteria = [""]
categories = ["Papers"]   #taxonomy
diseases = ["COVID-19"]
#series = [""]   # subset of series taxonomy
#tags = [""]   # taxonomy
viruses = ["SARS-CoV-2"]

#weight = ""

#draft = true

featured = true    # only for some themes
#math = true   # only for some themes
thumbnail = "images/building.png"   # only for some themes
+++

These are the research papers worth reading but often ignored or forgotten.

<!--more-->

