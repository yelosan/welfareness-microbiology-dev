+++
  title = "About: Diseases Central @Welfareness"
  description = "Diseases Central @Welfareness is a hub for the latest information about various diseases."
  summary = "Diseases Central @Welfareness is a hub for the latest information about various diseases."    # for summary text in list displays
  author = "Welfareness"

  date = "2020-08-10T06:01:00+08:00"
  #expiryDate = ""
  #lastmod = ""    # automatic, do not use unless necessary
  #publishDate = ""    # date var should be enough

  aliases = ["aboutus", "contact"]
  slug = "about"
  translationKey = "about"

  #bacteria = [""]
  #categories = [""]   #taxonomy
  #diseases = [""]
  keywords = ["diseases", "viruses", "bacteria", "microbes", "sickness", "plagues", "pandemic", "epidemic", "outbreak", "covid19", "covid-19", "coronavirus", "wuhan", "wuhanvirus", "sars"]   # meta keywords
  #series = [""]   # subset of series taxonomy
  #tags = [""]   # taxonomy
  #viruses = [""]

  #audio = ["/path/", "/path/"]
  #images = ["/path/", "/path/"]
  #videos = ["/path/", "/path"]

  #weight = ""

  #draft = true

  #featured = true    # only for some themes
  #math = true   # only for some themes
  #thumbnail = "images/building.png"   # only for some themes

  # https://gohugo.io/content-management/front-matter/#front-matter-cascade
  #[cascade]
  #  banner = "/path/"
+++

**Diseases Central @Welfareness** is our hub for gathering the latest information about various diseases. This idea started due to the numerous information circulating online, in televisions and radios, and in whispers, about the 2020 pandemic disease called *COVID-19*. Information which truly matters almost always end up forgotten and it is not easy to find it again when the information has become relevant.

Through **Diseases Central @Welfareness** we are hoping to collect these often forgotten information into one place. We will start with *COVID-19* as we are currently going through this pandemic. Eventually, we will also collect information about the other diseases plaguing humanity, we might also include diseases only found in animals if the need arises.

## What we are not
We are not a new repository for research papers nor are we an archive system. We also can not and we will not house every single available information, otherwise it will defeat our objective on concentrating on information that matters when it matters.

## What we can guarantee
Every information posted will have citations to let the reader decide for themselves if we understood the paper correctly. We may also link to third-party news and articles (meaning, not a research paper) but does not necessarily mean we share all of or any of what they wrote (unless it came from us too).
